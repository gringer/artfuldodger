aRtful dodgeR -- an R/Shiny problem solver
========

*Note: you must enter your API key into the API Key tab. This will create a file `apikey.txt` and automatically load next time.*

Quick Start
---

1. Click "Quick Load"
2. Interact with the problem vertices
3. Click "Download Pose"

Interaction
---

Click a vertex to select. Multiple vertices can be selected by holding
<Shift> and selecting vertices, or by dragging a rectangle around the
vertices to select.

Click again to deselect the vertices and shift the mean vertex
position to the clicked location.

Hold <Ctrl> and click on a point to create or remove a control point
(see below).

Hold <Ctrl> and drag a rectangle to zoom into a particular plot area.

Actions usually act on the selected vertices, or all vertices if none are
selected.

Hold <Alt> to start length search mode. Arrows left and right change
the target hole wall that is matched. Green figure edges identify
edges that match the target wall length within the specified epsilon
tolerance.

Keys
---

* **U**        : Undo the last movement.
* **<Escape>** : Cancel the current selection (or control points).

* **Arrow**           :  nudge the selected vertices one pixel.
* **<Shift> + Arrow** :  nudge the selected vertices five pixels.

* **H** : flip the vertices horizontally
* **V** : flip the vertices vertically
* **E** : attempt to shift the vertices towards a place that reduces
  elastic strain.

* **-** [minus] : zoom out (i.e. cancel the graph range).

Control Points
---

Up to two control points can be set for modifying rotation, flipping,
and [eventually, but not yet implemented] bonuses. When two control
points are set, the point of interaction is set to be the midpoint
between the two points.

When two points are chosen for flipping, the line of reflection is
set to between the two points. The meaning of horizontal and vertical
flip is changed to be relative to a pre-rotation around this line. In
this sense, a horizontal flip will reflect points along the control
point line, whereas a vertical flip will reflect points perpendicular
to the control point line.

Note on Bonuses
---

The "Add Bonus" input accepts a semicolon-separated list of bonuses
and metadata to apply to the pose file. The first item is the bonus
name, the second item is the problem that the bonus was achieved on.

For the "BREAK_A_LEG" bonus, the third and fourth elements are the
vertices describing an edge to break in half. User interface support
for BREAK_A_LEG is not yet implemented, so this does a simple break at
the half-way point, rounding the point location to the nearest
integer.

Example:

    BREAK_A_LEG,4,3,2;
